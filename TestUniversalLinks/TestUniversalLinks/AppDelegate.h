//
//  AppDelegate.h
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 30/10/15.
//  Copyright © 2015 Mohamed Fouad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

