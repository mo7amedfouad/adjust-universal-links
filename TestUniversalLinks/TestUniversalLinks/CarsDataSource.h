//
// Created by Mohamed Fouad on 14/01/16.
// Copyright (c) 2016 Mohamed Fouad. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RICar;

@interface CarsDataSource : NSObject
+ (NSArray *)cars;

+ (RICar *)carForName:(NSString *)carName;
@end