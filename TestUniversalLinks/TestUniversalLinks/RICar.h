//
//  RICar.h
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 14/01/16.
//  Copyright © 2016 Mohamed Fouad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RICar : NSObject <NSCopying>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *country;
@property (nonatomic) NSURL *url;

@end
