//
//  RICar.m
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 14/01/16.
//  Copyright © 2016 Mohamed Fouad. All rights reserved.
//

#import "RICar.h"

@implementation RICar
- (id)copyWithZone:(NSZone *)zone
{
    RICar *copy = [[[self class] allocWithZone:zone] init];

    if (copy != nil) {
        copy.name = self.name;
        copy.category = self.category;
        copy.country = self.country;
        copy.url = self.url;
    }

    return copy;
}

- (BOOL)isEqual:(id)other
{
    if (other == self)
        return YES;
    if (!other || ![[other class] isEqual:[self class]])
        return NO;

    return [self isEqualToCar:other];
}

- (BOOL)isEqualToCar:(RICar *)car
{
    if (self == car)
        return YES;
    if (car == nil)
        return NO;
    if (self.name != car.name && ![self.name isEqualToString:car.name])
        return NO;
    if (self.category != car.category && ![self.category isEqualToString:car.category])
        return NO;
    if (self.country != car.country && ![self.country isEqualToString:car.country])
        return NO;
    if (self.url != car.url && ![self.url isEqual:car.url])
        return NO;
    return YES;
}

- (NSUInteger)hash
{
    NSUInteger hash = [self.name hash];
    hash = hash * 31u + [self.category hash];
    hash = hash * 31u + [self.country hash];
    hash = hash * 31u + [self.url hash];
    return hash;
}


@end
