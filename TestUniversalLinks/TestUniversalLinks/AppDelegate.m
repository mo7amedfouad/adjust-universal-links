//
//  AppDelegate.m
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 30/10/15.
//  Copyright © 2015 Mohamed Fouad. All rights reserved.
//

#import "AppDelegate.h"
#import "Adjust.h"
#import "JLRoutes.h"
#import "RICar.h"
#import "DetailsViewController.h"
#import "CarsDataSource.h"

#define  kUniversalLinkBaseURL @"https://2925da33fcf39839458270a7a63c4380.ulink.adjust.com"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSLog(@"launchOptions %@", launchOptions);
    NSString *yourAppToken = @"40dhwdwmosao";
    NSString *environment = ADJEnvironmentSandbox;
    ADJConfig *adjustConfig = [ADJConfig configWithAppToken:yourAppToken
                                                environment:environment];
    [Adjust appDidLaunch:adjustConfig];
    [adjustConfig setLogLevel:ADJLogLevelDebug]; // enable all logging


    [JLRoutes addRoute:@"/cars/:carname" handler:^BOOL(NSDictionary *parameters) {

        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DetailsViewController *detailsViewController = (DetailsViewController *) [mainStoryboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
        detailsViewController.car = [CarsDataSource carForName:parameters[@"carname"]];

        UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
        [navigationController pushViewController:detailsViewController animated:YES];
        return YES;
    }];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [Adjust appWillOpenUrl:url];
    return [JLRoutes routeURL:url];
}

- (BOOL) application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
  restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler
{
    [Adjust appWillOpenUrl:userActivity.webpageURL];

    NSLog(@"%@",userActivity.userInfo);
    NSURL *deepLink = [NSURL URLWithString:@"RICars://cars"];

    if ([userActivity.webpageURL.absoluteString hasPrefix:kUniversalLinkBaseURL]) {
        //Adjust universal link
        // "https://2925da33fcf39839458270a7a63c4380.ulink.adjust.com/ulink/cars/bmw?adjust_reftag=cGM8WTzu7kRXf&adjust_redirect=https%3A%2F%2Fitunes.apple.com%2Fapp%2Fid23%3Fmt%3D8"
        deepLink = [deepLink URLByAppendingPathComponent:userActivity.webpageURL.lastPathComponent];
    } else {
        //Spotlight search
        NSString *carName = userActivity.title;
        deepLink = [deepLink URLByAppendingPathComponent:carName];
    }

    return [JLRoutes routeURL:deepLink];

}

- (void)adjustAttributionChanged:(ADJAttribution *)attribution
{
    NSLog(@"attribution %@", attribution);
}
@end
