//
//  DetailsViewController.m
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 14/01/16.
//  Copyright © 2016 Mohamed Fouad. All rights reserved.
//

#import "DetailsViewController.h"
#import "RICar.h"
#import <MobileCoreServices/UTCoreTypes.h>

@import CoreSpotlight;

@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *carImageView;
@end

@implementation DetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.car.name;
    self.carImageView.image = [UIImage imageNamed:self.car.name];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupUserActivityForCar:self.car];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.userActivity resignCurrent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupUserActivityForCar:(RICar *)car
{
    NSString *adjustURL = [NSString stringWithFormat:@"%@?adjust_tracker=6rkbdo&adjust_campaign=%@", car.url, car.category];
    NSURL *url = [NSURL URLWithString:adjustURL];

    self.userActivity = [[NSUserActivity alloc] initWithActivityType:@"com.cars.browsing"];
    self.userActivity.title = car.name;
    self.userActivity.userInfo = @{@"name" : car.name};
    self.userActivity.webpageURL = url;
    self.userActivity.eligibleForSearch = YES;
    self.userActivity.eligibleForPublicIndexing = YES;
    self.userActivity.eligibleForHandoff = YES;
    self.userActivity.requiredUserInfoKeys = [NSSet setWithArray:self.userActivity.userInfo.allKeys];

    CSSearchableItemAttributeSet *attributeSet = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *) kUTTypeImage];
    attributeSet.title = car.name;
    attributeSet.identifier = car.name;
    attributeSet.thumbnailData = UIImagePNGRepresentation([UIImage imageNamed:self.car.name]);
    self.userActivity.contentAttributeSet = attributeSet;

    [self.userActivity becomeCurrent];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
