//
//  DetailsViewController.h
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 14/01/16.
//  Copyright © 2016 Mohamed Fouad. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RICar;

@interface DetailsViewController : UIViewController
@property (nonatomic) RICar *car;
@end
