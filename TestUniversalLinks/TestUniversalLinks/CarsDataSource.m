//
// CarsDataSource
// Mohamed Fouad
//
//  Created by Mohamed Fouad on 14/01/16.
// Copyright (c) 2016 Mohamed Fouad. All rights reserved.
//
#import "CarsDataSource.h"
#import "RICar.h"

@interface CarsDataSource ()
@end

@implementation CarsDataSource
+ (NSArray *)cars
{
    static NSArray *storedCars;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        RICar *bmw = [[RICar alloc] init];
        bmw.name = @"bmw";
        bmw.category = @"super_car";
        bmw.country = @"Germany";
        bmw.url = [[NSURL alloc] initWithString:@"http://www.bmw.com/com/en/"];

        RICar *ferrari = [[RICar alloc] init];
        ferrari.name = @"ferrari";
        ferrari.category = @"super_duper_car";
        ferrari.country = @"Italy";
        ferrari.url = [[NSURL alloc] initWithString:@"http://www.ferrari.com/en_en/"];
        storedCars = @[bmw, ferrari];
    });
    return storedCars;
}

+ (RICar *)carForName:(NSString *)carName
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", carName];
    return [[[[self cars] filteredArrayUsingPredicate:predicate] firstObject] copy];
}

@end