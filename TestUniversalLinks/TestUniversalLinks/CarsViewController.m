//
//  CarsViewController.m
//  TestUniversalLinks
//
//  Created by Mohamed Fouad on 30/10/15.
//  Copyright © 2015 Mohamed Fouad. All rights reserved.
//

#import "CarsViewController.h"
#import "Adjust.h"
#import "RICar.h"
#import "CarsDataSource.h"
#import "DetailsViewController.h"
#import "JLRoutes.h"

@interface CarsViewController ()
@end

@implementation CarsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [CarsDataSource cars].count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"car_cell"];
    RICar *car = [CarsDataSource cars][(NSUInteger) indexPath.row];
    cell.textLabel.text = car.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    RICar *car = [CarsDataSource cars][(NSUInteger) indexPath.row];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"RICars://cars/%@", car.name]];
    [JLRoutes routeURL:url];

}

@end
